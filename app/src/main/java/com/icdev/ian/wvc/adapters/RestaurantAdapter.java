package com.icdev.ian.wvc.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.vipulasri.timelineview.TimelineView;
import com.icdev.ian.wvc.R;
import com.icdev.ian.wvc.models.Restaurant;
import com.icdev.ian.wvc.models.TimeLineModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by HP-HP on 05-12-2015.
 */
public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapter.TimeLineViewHolder> {

    private List<Restaurant> mFeedList;
    private Context mContext;
    private OnItemClicked onClick;

    public interface OnItemClicked {
        void onItemClick(int position);
    }
    public RestaurantAdapter(List<Restaurant> feedList) {
        mFeedList = feedList;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater mLayoutInflater = LayoutInflater.from(mContext);
        View view;

        view = mLayoutInflater.inflate(R.layout.adapter_restaurant, parent, false);

        return new TimeLineViewHolder(view, viewType);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(TimeLineViewHolder holder, final int position) {

        holder.name.setText(mFeedList.get(position).getName());
        holder.phone.setText("Phone: " + mFeedList.get(position).getPhone());
        holder.distance.setText("Distance: " + mFeedList.get(position).getDistance());
        holder.type.setText("Type: " + mFeedList.get(position).getType());
        holder.value.setText("Value: " + mFeedList.get(position).getValue());
        holder.localization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.onItemClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return (mFeedList!=null? mFeedList.size():0);
    }

    class TimeLineViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView phone;
        TextView type;
        TextView value;
        TextView distance;
        TextView localization;


        TimeLineViewHolder(View itemView, int viewType) {
            super(itemView);
            name = itemView.findViewById(R.id.name_restaurant);
            phone = itemView.findViewById(R.id.phone_restaurant);
            type = itemView.findViewById(R.id.type_restaurant);
            value = itemView.findViewById(R.id.value_restaurant);
            distance = itemView.findViewById(R.id.distance_restaurant);
            localization = itemView.findViewById(R.id.location_restaurant);

        }
    }

    public void setOnClick(OnItemClicked onClick)
    {
        this.onClick=onClick;
    }
}