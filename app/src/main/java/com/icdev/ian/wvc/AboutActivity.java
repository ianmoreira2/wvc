package com.icdev.ian.wvc;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("About");

        TextView textView =findViewById(R.id.text_about);
        textView.setText(Html.fromHtml("<b>Ian Andrade Moreira</b> is a student of computer science at UESC. The main activities of his are development of application for android\n" +
                "     and work with clusters at NBCGIB - ianmoreira80@gmail.com"));

        TextView camila = findViewById(R.id.text_about2);
        camila.setText(Html.fromHtml("<b>Camila Dias Barbosa</b> is also a student of computer science at UESC. The main activity of her is web development at NIT: Núcleo de Inovação Tecnológica - cdibarbosa2017@gmail.com"));
    }
}
