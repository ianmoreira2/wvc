package com.icdev.ian.wvc;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class SponsorsActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponsors);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Sponsors");
        findViewById(R.id.image_atos).setOnClickListener(this);
        findViewById(R.id.image_ppgmc).setOnClickListener(this);
        findViewById(R.id.image_ima).setOnClickListener(this);
        findViewById(R.id.image_cepedi).setOnClickListener(this);
        findViewById(R.id.image_uesc).setOnClickListener(this);
        findViewById(R.id.image_nbcgib).setOnClickListener(this);
        findViewById(R.id.image_sbc).setOnClickListener(this);
        findViewById(R.id.image_itau).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Uri uri = Uri.parse("http://www.uesc.br/"); // missing 'http://' will cause crashed

        switch (v.getId()){

            case R.id.image_ppgmc:
                uri = Uri.parse("http://nbcgib.uesc.br/ppgmc/");
                break;
            case  R.id.image_cepedi:
                uri = Uri.parse("http://www.cepedi.org.br/");
                break;
            case R.id.image_ima:
                break;
            case  R.id.image_uesc:
                break;
            case  R.id.image_sbc:
                uri = Uri.parse("http://www.sbc.org.br/");
                break;
            case  R.id.image_nbcgib:
                uri = Uri.parse("http://nbcgib.uesc.br/");
                break;
            case  R.id.image_itau:
                uri = Uri.parse("https://www.itau.com.br/");
                break;
            case  R.id.image_atos:
                uri = Uri.parse("https://atos.net/pt-br/brasil");
                break;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
}
