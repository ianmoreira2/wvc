package com.icdev.ian.wvc;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

public class OrganizationActivity extends AppCompatActivity {

    private  TextView description;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_organization);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Organization");

        description = findViewById(R.id.textViewOrganization);
        description.setText(Html.fromHtml("<div><h1>Organization</h1>\n" +
                "<div >\n" +
                "<div >\n" +
                "<h2>Local Organizing Committee</h2>\n" +
                "<ul >\n" +
                "<p>Paulo Eduardo Ambrósio (UESC) – GENERAL CHAIR</p>\n" +
                "<p>Marta Magda Dornelles (UESC) – PROGRAM CHAIR</p>\n" +
                "<p>Marcelo Ossamu Honda (UESC)</p>\n" +
                "<p>Leard de Oliveira Fernandes (UESC)</p>\n" +
                "<p>Francisco Bruno Souza Oliveira (UESC)</p>\n" +
                "<p>César Alberto Bravo Pariente (UESC)</p>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div> \n" +
                "<div>\n" +
                "<h2>Steering Committee</h2>\n" +
                "<ul>\n" +
                "<p>Mauricio Cunha Escarpinati (UFU) – CHAIR</p>\n" +
                "<p>Aparecido Nilceu Marana (UNESP)</p>\n" +
                "<p>Bruno Marques Ferreira da Silva (UFRN)</p>\n" +
                "<p>Evandro Luís Linhari Rodrigues (USP)</p>\n" +
                "<p>Hemerson Pistori (UCDB)</p>\n" +
                "<p>Marcelo Andrade da Costa Vieira (USP)</p>\n" +
                "<p>Mauricio Marengoni (Mackenzie)</p>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>"));
    }
}
