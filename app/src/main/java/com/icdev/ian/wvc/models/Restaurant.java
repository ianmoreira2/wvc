package com.icdev.ian.wvc.models;

public class Restaurant {
    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public String getDistance() {
        return distance;
    }

    public String getPhone() {
        return phone;
    }

    public String getType() {
        return type;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLog() {
        return log;
    }

    String name;

    public Restaurant(String name, Double lat, Double log, String phone, String distance,  String type,  String value) {
        this.name = name;
        this.value = value;
        this.distance = distance;
        this.phone = phone;
        this.type = type;
        this.lat = lat;
        this.log = log;
    }

    String value;
    String distance;
    String phone;
    String type;
    Double lat;
    Double log;

}
