package com.icdev.ian.wvc;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.icdev.ian.wvc.adapters.TimeLineAdapter;
import com.icdev.ian.wvc.models.TimeLineModel;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("ValidFragment")
public class TabProgramationFragment extends Fragment {

    RecyclerView mRecyclerView;
    private List<TimeLineModel> mDataList = new ArrayList<>();
    private TimeLineAdapter mTimeLineAdapter;
    Context mContext;
//    private ProgAdpter mAdapter;
    String dia;

    @SuppressLint("ValidFragment")
    public TabProgramationFragment(String dia) {
        this.dia = dia;
    }

    //This is our tablayout
    //Overriden method onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        View v = inflater.inflate(R.layout.frament_tab1, container, false);
        mContext = container.getContext();
        mRecyclerView = v.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(layoutManager);

//?        mDataList.add(new TimeLineModel());
        mDataList = MainActivity.database.getArrayProgramation(dia);
        // Adiciona o adapter que irá anexar os objetos à lista.
        // Está sendo criado com lista vazia, pois será preenchida posteriormente.
        ArrayList<TimeLineAdapter> array = new ArrayList<>();
        mTimeLineAdapter = new TimeLineAdapter(mDataList);
        mTimeLineAdapter.setHasStableIds(true);
        mRecyclerView.setAdapter(mTimeLineAdapter);

        return v;
    }
}
