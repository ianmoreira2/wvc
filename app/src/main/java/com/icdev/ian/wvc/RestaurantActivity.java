package com.icdev.ian.wvc;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.icdev.ian.wvc.adapters.RestaurantAdapter;
import com.icdev.ian.wvc.models.Restaurant;

import java.util.ArrayList;
import java.util.List;

public class RestaurantActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, RestaurantAdapter.OnItemClicked {

    private GoogleMap mMap;
    LatLng ll = new LatLng(-14.785822, -39.037910);
    RecyclerView mRecyclerView;
    Context mContext;
    List<Restaurant> all;
    private ArrayList<Marker> mMarkerArray = new ArrayList<Marker>();


    RestaurantAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_restaurant);
        mapFragment.getMapAsync(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Restaurant");
        mContext = this;
        mRecyclerView = findViewById(R.id.recyclerView_restaurant);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(layoutManager);

        all = MainActivity.database.getArrayRestaurant();

        mAdapter = new RestaurantAdapter(all);
        mRecyclerView.setAdapter(mAdapter);
//        mAdapter.setHasStableIds(true);
        mAdapter.setOnClick(this);
//        mMarkerArray.get(0).showInfoWindow();
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mMarkerArray.get(0).getPosition(), 17));

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setUpMap();

//        mMap.setOnMarkerClickListener(this);
    }

    private void setUpMap() {
        int size = all.size();
        for(int i = 0; i < size; i++){
            Log.d("vllll", all.get(i).getName()+", "+all.get(i).getLat()+", "+ all.get(i).getLog());
            mMarkerArray.add( mMap.addMarker(new MarkerOptions().position(new LatLng(all.get(i).getLat(), all.get(i).getLog())).title(all.get(i).getName())));
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        // Add a marker in Sydney and move the camera
        mMap.setMyLocationEnabled(true);

        CameraUpdate location;
        CameraUpdate zoom;
        // Add a marker in Sydney and move the camera
        mMap.addMarker(new MarkerOptions().position(ll).title("CEPEDI")).showInfoWindow();
        mMap.moveCamera(CameraUpdateFactory.newLatLng(ll));
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

        zoom = CameraUpdateFactory.zoomTo(15);
        location = CameraUpdateFactory.newLatLngZoom(ll, 15);
        mMap.animateCamera(zoom, 2000, null);
        mMap.moveCamera(location);
    }
        @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    @Override
    public void onItemClick(int position) {
        mMarkerArray.get(position).showInfoWindow();
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mMarkerArray.get(position).getPosition(), 17));

    }
}
