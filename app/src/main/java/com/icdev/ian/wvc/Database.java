package com.icdev.ian.wvc;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.icdev.ian.wvc.models.Restaurant;
import com.icdev.ian.wvc.models.TimeLineModel;

import java.util.ArrayList;
import java.util.List;

public class Database extends SQLiteOpenHelper {
    public static final String NOMEDB = "programationDB";
    public static final String LOCALDB = "/data/data/com.icdev.ian.wvc/databases/";
    public static final int VERSION = 2;
    private Context mContext;
    private SQLiteDatabase mSQLiteDatabase;
    public Cursor cursor;

    public Database(Context context) {
        super(context, NOMEDB, null, VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void openDataBase() {
        String dbPath = mContext.getDatabasePath(NOMEDB).getPath();
//        Log.d("state", dbPath);
        if (mSQLiteDatabase != null && mSQLiteDatabase.isOpen()) {
            return;
        }

        mSQLiteDatabase = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READWRITE);

    }

    public void closeDatabase() {
        if (mSQLiteDatabase != null) {
            mSQLiteDatabase.close();
        }
    }

    public void open() {
        openDataBase();
        mSQLiteDatabase = this.getWritableDatabase();
    }

    public ArrayList<TimeLineModel> getArrayProgramation(String dia){
        ArrayList<TimeLineModel> arrayList = new ArrayList<>();

        try {
            String sql = "SELECT * FROM TBL_PROGRAMATION WHERE END_TIME lIKE '%"+ dia+"' ORDER BY ID ASC";
            cursor = mSQLiteDatabase.rawQuery(sql, null);
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                            Log.d("context", ""+cursor.getString(3)+"  "+ cursor.getString(4)+"  "+ cursor.getString(2)+"  "+ cursor.getString(1));
                        arrayList.add(new TimeLineModel(cursor.getString(3), cursor.getString(4), cursor.getString(2), cursor.getString(1), cursor.getString(5)));
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            // here you can catch all the exceptions
            e.printStackTrace();
        }
        return arrayList;

    }

    public ArrayList<Restaurant> getArrayRestaurant(){
        ArrayList<Restaurant> arrayList = new ArrayList<>();
        try {
            String sql = "SELECT * FROM TBL_RESTAURANTE ORDER BY ID ASC";
            cursor = mSQLiteDatabase.rawQuery(sql, null);
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        Log.d("context", cursor.getString(1)+"  "+cursor.getString(2)+",  "+ cursor.getString(3));
                        arrayList.add(new Restaurant(cursor.getString(1), cursor.getDouble(2),cursor.getDouble(3), cursor.getString(4), cursor.getString(5),cursor.getString(6), cursor.getString(7)));
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            // here you can catch all the exceptions
            e.printStackTrace();
        }
        return  arrayList;
    }

}
