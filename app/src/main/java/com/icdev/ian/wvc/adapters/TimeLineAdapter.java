package com.icdev.ian.wvc.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.vipulasri.timelineview.TimelineView;
import com.icdev.ian.wvc.R;
import com.icdev.ian.wvc.models.TimeLineModel;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by HP-HP on 05-12-2015.
 */
public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineAdapter.TimeLineViewHolder> {

    private List<TimeLineModel> mFeedList;
    private Context mContext;

    public TimeLineAdapter(List<TimeLineModel> feedList) {
        mFeedList = feedList;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater mLayoutInflater = LayoutInflater.from(mContext);
        View view;

        view = mLayoutInflater.inflate(R.layout.adpter_time_line, parent, false);

        return new TimeLineViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(TimeLineViewHolder holder, int position) {

        holder.mTitle.setText(mFeedList.get(position).getTitle());

        if(mFeedList.get(position).getImage() != null){
            Context context = holder.imageView.getContext();
            int id = context.getResources().getIdentifier(mFeedList.get(position).getImage(), "mipmap", context.getPackageName());
            holder.imageView.setImageResource(id);
            holder.imageView.setVisibility(View.VISIBLE);
        }


        if(mFeedList.get(position).getAuthor().equals("")){
            holder.mAuthor.setVisibility(View.GONE);
            holder.mAuthor.setText("");
        }else {
            holder.mAuthor.setText(mFeedList.get(position).getAuthor());
        }

        if(mFeedList.get(position).getStartTime().equals("")) {
            holder.mDate.setVisibility(View.GONE);
            holder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.ic_vertical_align_center_black_24dp), ContextCompat.getColor(mContext, R.color.colorPrimary));
//            holder.mTimelineView.setVisibility(View.INVISIBLE);
        }else {
            holder.mDate.setText(mFeedList.get(position).getStartTime().split(" ")[0]);
            DateFormat dateF =new SimpleDateFormat("HH:mm dd/MM/yyyy");
            String inputStart = mFeedList.get(position).getStartTime();
            String inputEnd = mFeedList.get(position).getEndTime();
            Date dateStart = null;
            Date dateEnd = null;
            try {
                dateStart = dateF.parse(inputStart);
                dateEnd = dateF.parse(inputEnd);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long millisecondsStart = dateStart.getTime();
            long millisecondsEnd = dateEnd.getTime();
            Date now = new Date();
//        long millisecondsFromNow = milliseconds - (new Date()).getTime();
//        Log.d("date", ""+milliseconds+ "   "+ now.getTime());
            if(millisecondsStart < now.getTime() && now.getTime() < millisecondsEnd) {
                holder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.ic_marker_active), ContextCompat.getColor(mContext, R.color.colorPrimary));
            }
            else if(millisecondsStart < now.getTime()) {
                holder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.ic_marker_inactive),  ContextCompat.getColor(mContext, android.R.color.darker_gray));
            } else {
                holder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.ic_marker), ContextCompat.getColor(mContext, R.color.colorPrimary));
            }
        }

//        if(mFeedList.get(position).getStartTime().equals("")) {

//        }
    }

    @Override
    public int getItemCount() {
        return (mFeedList!=null? mFeedList.size():0);
    }

    class TimeLineViewHolder extends RecyclerView.ViewHolder {

        TextView mDate;
        TextView mTitle;
        TextView mAuthor;
        ImageView imageView;
        TimelineView mTimelineView;

        TimeLineViewHolder(View itemView, int viewType) {
            super(itemView);

            mTimelineView = itemView.findViewById(R.id.time_marker);
            mAuthor = itemView.findViewById(R.id.text_timeline_author);
            mDate = itemView.findViewById(R.id.text_timeline_date);
            mTitle = itemView.findViewById(R.id.text_timeline_title);
            imageView = itemView.findViewById(R.id.image_timeline);

        }
    }

}