package com.icdev.ian.wvc.models;

public class TimeLineModel {

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String startTime;

    public TimeLineModel(String startTime, String endTime, String title, String author, String image) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.title = title;
        this.author = author;
        this.image = image;
    }

    public String endTime;
    public String title;
    public String author;

    public String getImage() {
        return image;
    }

    public String image;
}
