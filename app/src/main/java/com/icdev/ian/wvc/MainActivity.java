package com.icdev.ian.wvc;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    @SuppressLint("StaticFieldLeak")
    public static Database database;
    FloatingActionButton fab;
    FloatingActionButton fab1;
    FloatingActionButton fab2;
    FloatingActionButton fab3;
    boolean isFABOpen=false;
    public static String FLAG="It the start";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer =findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        checkPermission();
        initDatabase();
        populateList();
        fab = findViewById(R.id.fab);
        fab1 = findViewById(R.id.fab1);
        fab2 = findViewById(R.id.fab2);
        fab3 = findViewById(R.id.fab3);
        fab1.setOnClickListener(this);
        fab2.setOnClickListener(this);
        fab3.setOnClickListener(this);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFABOpen){
                    showFABMenu();
                }else{
                    closeFABMenu();
                }
            }
        });
        Thread t=new Thread(new Runnable() {
            @Override
            public void run() {

                SharedPreferences sharedPreferences = getSharedPreferences(FLAG, Context.MODE_PRIVATE);

                if(sharedPreferences.getBoolean(FLAG,true)){
                    startActivity(new Intent(MainActivity.this,DefaultIntro.class));
                    SharedPreferences.Editor e=sharedPreferences.edit();
                    e.putBoolean(FLAG,false);
                    e.apply();
                }
            }
        });
        t.start();
    }
    private void showFABMenu(){
        isFABOpen=true;

        fab.setImageDrawable(this.getDrawable(R.drawable.ic_expand_less_white_24dp));
        fab1.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        fab2.animate().translationY(-getResources().getDimension(R.dimen.standard_105));
        fab3.animate().translationY(-getResources().getDimension(R.dimen.standard_155));
    }

    private void closeFABMenu(){
        isFABOpen=false;
        fab.setImageDrawable(this.getDrawable( R.drawable.ic_expand_more_white_24dp));
        fab1.animate().translationY(0);
        fab2.animate().translationY(0);
        fab3.animate().translationY(0);
    }
    private void initDatabase() {

        database = new Database(this);
        File base = getApplicationContext().getDatabasePath(Database.NOMEDB);
        if (!base.exists()) {
            database.getReadableDatabase();
                    copyDatabase();
        }
    }

    private void populateList() {
        database = new Database(this);
        database.open();
    }

    private void copyDatabase() {
        try {
            InputStream inputStream = getApplicationContext().getAssets().open(Database.NOMEDB);
            String outFile = Database.LOCALDB + Database.NOMEDB;
            OutputStream outputStream = new FileOutputStream(outFile);
            byte[] buff = new byte[1024];
            int legth;
            while ((legth = inputStream.read(buff)) > 0) {
                outputStream.write(buff, 0, legth);
            }

            outputStream.flush();
            outputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        database.closeDatabase();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity.this,DefaultIntro.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent generic =null;
        if (id == R.id.nav_place) {
            generic = new Intent(this, MapsActivity.class);
        } else if (id == R.id.nav_programation) {
            generic = new Intent(this, ProgActivity.class);
        } else if (id == R.id.nav_organization) {
            generic = new Intent(this, OrganizationActivity.class);
        } else if (id == R.id.nav_par) {
            generic = new Intent(this, SponsorsActivity.class);
        } else if (id == R.id.nav_restaurant) {
            generic = new Intent(this, RestaurantActivity.class);
        } else if (id == R.id.nav_uesc) {
//            generic = new Intent(this, UescActivity.class);
            Uri uri = Uri.parse("http://www.uesc.br/"); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        } else if (id == R.id.nav_register) {
            generic = new Intent(this, PlaceActivity.class);
        } else if (id == R.id.nav_contacts) {
            generic = new Intent(this, ContactsActivity.class);
        } else if (id == R.id.nav_about) {
            generic = new Intent(this, AboutActivity.class);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        if (generic != null) {
            startActivity(generic);
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fab1:
                startActivity(new Intent(this, RestaurantActivity.class));
                break;
            case R.id.fab2:
                startActivity(new Intent(this, ProgActivity.class));
                break;
            case R.id.fab3:
                startActivity(new Intent(this, MapsActivity.class));
                break;

        }
    }
}
